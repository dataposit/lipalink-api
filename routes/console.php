<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('eazzy:keys', function () {
    $this->comment(\App\Eazzy::create_keys());
})->describe('Create keys for Eazzy pay.');

Artisan::command('eazzy:authenticate', function () {
    $this->comment(\App\Eazzy::authenticate());
})->describe('Authenticate to eazzy platform.');

Artisan::command('test:sms', function() {
	$response = \App\SMS::testfromconsole();
	$this->info($response);
})->describe('Test send SMS notifications');

Artisan::command('create:shortcode', function() {
	$mpesa = new \App\Mpesa(env('MPESA_URL'));
	$response = $mpesa->testRegisterCallback();
	$this->info($response);
})->describe('Create shortcodes');

Artisan::command('synch:transactions', function () {
	$synch = new \App\Payment;
	$str = $synch->pushTransactionToNav();
	$str .= "\n==> Completed synching pending Sales Agents " . date('d/m/Y h:i:s a', time()). "\n";
	$this->info($str);
})->describe('Synch the transactions');

Artisan::command('nav:transactions', function (){
	$synch = new \App\Payment;
	$str = $synch->getNavTransactions();
	$str .= "\n Returned transactions\n";
	$this->info($str);
})->describe('Get synched transactions');

Artisan::command('reset:transactions {organization}', function($organization) {
	$reset = \App\Random::resetOrganizationTransactions($organization);
	$this->info($reset);
})->describe('Reset organization transactions');

Artisan::command('test:register', function() {
	$class = new \App\Mpesa;
	$str = $class->testing();
	$str .= "\n Testing callbacks";
	$this->info($str);
})->describe('Test register callbacks');
