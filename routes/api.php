<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['namespace' => 'App\\Api\\V1\\Controllers'], function(Router $api) {
        $api->group(['prefix' => 'auth'], function(Router $api) {
            $api->post('signup', 'SignUpController@signUp');
            $api->post('login', 'LoginController@login');

            $api->post('recovery', 'ForgotPasswordController@sendResetEmail');
            $api->post('reset', 'ResetPasswordController@resetPassword');

            $api->post('logout', 'LogoutController@logout');
            $api->post('refresh', 'RefreshController@refresh');
            $api->get('me', 'UserController@me');
        });

        $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
            $api->get('protected', function() {
                return response()->json([
                    'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
                ]);
            });

            $api->get('refresh', [
                'middleware' => 'jwt.refresh',
                function() {
                    return response()->json([
                        'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                    ]);
                }
            ]);

            $api->group(['prefix' => 'mpesa'], function(Router $api) {
                $api->post('registercallback', 'MpesaController@registerCallback');

                /*
                 * Test routes
                 */
                $api->post('simulatepayment', 'MpesaController@simulatepayment');
            });
        });

        $api->post('stkpush', 'MpesaController@stkpush');
        $api->get('stkpush', function() {
            return response()->json([
                'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
            ]);
        });

        $api->group(['prefix' => 'callbacks'], function(Router $api) {
            $api->post('queue_timeout', 'MpesaController@queue_timeout');
            $api->post('balance', 'MpesaController@balance');
            $api->post('result', 'MpesaController@result');
            $api->post('confirmtransaction', 'MpesaController@confirm');
            $api->post('validatetransaction', 'MpesaController@validate_transaction');
            $api->post('pushcallback', 'MpesaController@callback');
        });

        $api->get('/', function(){
            return response()->json([
                'message' => 'This is the root of the API.'
            ]);
        });

        $api->get('hello', function() {
            return response()->json([
                'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
            ]);
        });

        $api->post('hello', function(\Dingo\Api\Http\FormRequest $request) {
            return response()->json($request->all());
        });
    });
});
