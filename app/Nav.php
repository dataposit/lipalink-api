<?php

namespace App;

use App\Mail\NavSynchFailure;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Facades\Mail;

class Nav
{
    /**
     * URI of the Navision server
     */
    private $NTLM_URI = NULL;

    /**
     * URI which the service runs from
     * @integer
     */
    private $NTLM_PORT = NULL;

    private $NTLM_SERVICE = NULL;
    private $NTLM_SOAP = FALSE;
    private $NTLM_ODATA = FALSE;
    private $NTLM_ODATA_VERSION = NULL;
    private $NTLM_COMPANY = 'CRONUS';
    private $NTLM_USERNAME = NULL;
    private $NTLM_PASSWORD = NULL;

    function __construct($data = null)
    {
        $data = (object) $data;
        $this->NTLM_URI = $data->NTLM_URI ?? '';
        $this->NTLM_PORT = $data->NTLM_PORT ?? '';
        $this->NTLM_SERVICE = $data->NTLM_SERVICE ?? '';
        $this->NTLM_SOAP = $data->NTLM_SOAP ?? $this->NTLM_SOAP;
        $this->NTLM_ODATA = $data->NTLM_ODATA ?? $this->NTLM_ODATA;
        $this->NTLM_ODATA_VERSION = $data->NTLM_ODATA_VERSION ?? $this->NTLM_ODATA_VERSION;
        $this->NTLM_ODATA_VERSION = trim($this->NTLM_ODATA_VERSION);
        $this->NTLM_COMPANY = $data->NTLM_COMPANY ?? $this->NTLM_COMPANY;
        $this->NTLM_USERNAME = $data->NTLM_USERNAME ?? '';
        $this->NTLM_PASSWORD = $data->NTLM_PASSWORD ?? '';
    }

    public function getResource($service, $resource = NULL)
    {
        $uri = $this->buildURL($service, $resource);
        
        try {
            $client = new Client();
            $apiRequest = $client->request('GET', $uri,[
                    'auth' => $this->getNavAuthData(), //If authentication required
                    'debug' => false //If needed to debug   
              ]);
            
            $content = json_decode($apiRequest->getBody()->getContents());
            return $content->value;   
        } catch (Exception $e) {
            return $e;   
        }
    }

    public function createResource($service, $data)
    {
        $uri = $this->buildURL($service);
        // return $uri;
        try {
            $client = new Client();
            $apiRequest = $client->request('POST', $uri,[
                    'auth' => $this->getNavAuthData(), //If authentication required
                    'debug' => false,//If needed to debug
                    'json' => $data,
              ]);
            $content = json_decode($apiRequest->getBody()->getContents());
            return $content;   
        } catch (ClientException $e) { //Client exceptions
            $data = (object)[
                    'status' => 400,
                    'error' => Psr7\str($e->getResponse())
                ];
            Mail::to(['joshua.bakasa@dataposit.co.ke'])->send(new NavSynchFailure($data));
            return $data;
        } catch (RequestException $e) { // In the event of a networking error (connection timeout, DNS errors, etc.)
            if ($e->hasResponse()) {
                $message = Psr7\str($e->getResponse());
            } else {
                $message = 'Could not connect to the specified host';
            }
            $data = (object)[
                'status' => 500,
                'error' => $message
            ];
            Mail::to(['joshua.bakasa@dataposit.co.ke'])->send(new NavSynchFailure($data));
            return $data;
        }
    }

    public function updateResource($service, $resource = NULL)
    {
        $resource = $this->getResource($service, $resource);
        
        return $resource;
    }

    private function getNavAuthData()
    {
        return [$this->NTLM_USERNAME, $this->NTLM_PASSWORD, 'ntlm'];
    }


    private function buildURL($service, $resource = NULL)
    {
        $url = '';
        if ($this->NTLM_URI === NULL)
            return NULL;
        $url = $this->addPortToURL();

        // // Add Service to the URI
        $url .= $this->NTLM_SERVICE . '/';

        // // Add web service type to the URI
        $url = $this->addWebServiceType($url);

        // Add company to the URI
        $url = $this->addCompanyToUri($url) . $service;

        $url .= $this->buildQueryFilters($resource);

        return $url;
    }

    private function addPortToURL()
    {
        if ($this->NTLM_PORT === NULL)
            return $this->NTLM_URI;
        return $this->NTLM_URI . ':' . $this->NTLM_PORT . '/';
    }

    private function addWebServiceType($url)
    {
        if ($this->NTLM_ODATA) { // Odata method of data transfer
            $url .= 'OData';
            if ($this->NTLM_ODATA_VERSION)
                $url .= $this->NTLM_ODATA_VERSION;
            $url .= '/';
        } else if ($this->NTLM_SOAP) {
            $url .= '/';
        }
        return $url;
    }

    private function addCompanyToUri(&$url)
    {
        $url .= "Company('{$this->NTLM_COMPANY}')/";
        return $url;
    }

    private function buildQueryFilters($filter)
    {
        $query = "?";
        if ($filter != NULL) {
            $loops = sizeof($filter);
            $count = 0;
            $query .= "\$filter=";
            foreach ($filter as $key => $value) {
                $count++;
                $query .= "{$key} eq '{$value}'";
                if ($count < $loops)
                    $query .= " and ";
            }
            $query .= "&";
        }

        if($this->NTLM_ODATA_VERSION != 'V4')
            $query .= "\$format=json";

        return $query;
    }

    public function testMail()
    {
        $data = (object)[
                    'status' => 400,
                    'body' => 'This is an error message'
                ];
        Mail::to(['joshua.bakasa@dataposit.co.ke'])->send(new NavSynchFailure($data));
    }
}