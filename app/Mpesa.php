<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;

use \App\Payment;
use \App\Shortcode;

class Mpesa
{
	private $mpesa_url;

	private $apiTokenPrefix = 'api_token_';

	function __construct($class_data = null)
	{
		// $this->mpesa_url = 'https://api.safaricom.co.ke'; // For development only
		$this->mpesa_url = $class_data['mpesa_url'] ?? '';
	}

    public function registercallback($shortcode = null, $data = null) {
    	if (!isset($shortcode) || !isset($data) || !is_array($data))
    		return false;
    	$data = (object) $data;
		
		$client = new Client(['base_uri' => $this->mpesa_url]);
    	$response = $client->request('post', 'mpesa/c2b/v1/registerurl', [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->get_token($shortcode),
				'Accept' => 'application/json',
			],
			'http_errors' => true,
			'debug' => true,
			'json' => $data->body,
		]);

		$body = json_decode($response->getBody());
		return [
			'status' => $response->getStatusCode(),
			'body' => json_decode($response->getBody()),
		];
    }

    public function c2b($shortcode = null, $data = null)
    {
    	if (!isset($shortcode) || !isset($data) || !is_array($data))
    		return false;
    	$data = (object) $data;

    	$client = new Client(['base_uri' => $this->mpesa_url]);
    	$response = $client->request('post', 'mpesa/c2b/v1/simulate', [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->get_token($shortcode),
				'Accept' => 'application/json',
			],
			'http_errors' => false,
			'debug' => false,
			'json' => $data->body,
		]);

		$body = json_decode($response->getBody());
		return (object)[
			'status' => $response->getStatusCode(),
			'body' => json_decode($response->getBody()),
		];
    }

    public function b2b()
    {
    	if (!isset($shortcode) || !isset($data) || !is_array($data))
    		return false;
    	$data = (object) $data;

    	$client = new Client(['base_uri' => $this->mpesa_url]);
    	$response = $client->request('post', 'mpesa/b2b/v1/paymentrequest', [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->get_token($shortcode),
				'Accept' => 'application/json',
			],
			'http_errors' => false,
			'debug' => false,
			'json' => $data->body,
		]);
    }

    public function transactionstatus($shortcode = null, $data = null)
    {
  //   	if (!isset($shortcode) || !isset($data) || !is_array($data))
  //   		return false;
  //   	$data = (object) $data;

  //   	$client = new Client(['base_uri' => $this->mpesa_url]);
  //   	$response = $client->request('post', 'mpesa/c2b/v1/registerurl', [
		// 	'headers' => [
		// 		'Authorization' => 'Bearer ' . $this->get_token($shortcode),
		// 		'Accept' => 'application/json',
		// 	],
		// 	'http_errors' => false,
		// 	'debug' => false,
		// 	'json' => $data->body,
		// ]);

		// $body = json_decode($response->getBody());
		// return (object)[
		// 	'status' => $response->getStatusCode(),
		// 	'body' => json_decode($response->getBody()),
		// ];
    }

    /*
     * Enabler functions
     *
     */

	public function get_token($shortcode)
	{
		// if(!Cache::store('file')->has($this->apiTokenPrefix.$shortcode->shortcode)) {
			$this->authenticate($shortcode);
		// }
		return Cache::store('file')->get($this->apiTokenPrefix.$shortcode->shortcode);
	}

    public function authenticate($shortcode=null)
    {
        $client = new Client(['base_uri' => $this->mpesa_url]);

		$response = $client->request('get', '/oauth/v1/generate?grant_type=client_credentials', [
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . base64_encode($shortcode->consumer_key . ':' . $shortcode->consumer_secret),
			],
			'http_errors' => false,
			'debug' => false,
		]);
		
		if($response->getStatusCode() > 399)
			return null;

		$body = json_decode($response->getBody());
		// return $body;
		// return $body->access_token;
		$expires_in = $body->expires_in;
		return Cache::store('file')->put($this->apiTokenPrefix.$shortcode->shortcode, $body->access_token, (60*60));
    }

	// http://197.248.9.51:8080/api/callbacks/validate_transaction
    public static function sim($mobile=null, $amount=null, $shortcode=null)
    {
    	$shortcode = Shortcode::where('shortcode', $shortcode)->first();
    	$payment = new Payment;
    	$payment->payment_amount = $amount;
    	$payment->mobile = $mobile;
    	$payment->user_id = 1;
    	$payment->shortcode_id = $shortcode->shortcode;
    	$payment->save();

    	return $payment;
    }



	private static function get_security_credential($initiator=null)
	{
		// return env('SECURITY_CREDENTIAL');
		// if(!$initiator) $initiator = env('MPESA_INITIATOR');
		if(!$initiator) $initiator = env('MPESA_SEC_CRED');
		$public_key = file_get_contents(public_path('mpesa_key.txt'));
		openssl_public_encrypt($initiator, $encrypted, $public_key, OPENSSL_PKCS1_PADDING);
		return base64_encode($encrypted);
	}

	public function createShortCodes()
	{		
		$shortcodes = [
						// ["orgnaization_name" => 'City Eye Hospital Kitengela', "orgnaization_shortcode" => '904861']
						['orgnaization_name' => 'City Eye Hospital Limited', 'orgnaization_shortcode' => '977787']
						// ['orgnaization_name' => 'City Eye Hospital Limited', 'orgnaization_shortcode' => '583848']
					];

		$secrets = [
					// ['Key' => 'JXyFXjkug2WWO0CQswJUDvPo6clyxCXG', 'Secret' => 'TNkwp3tHQCTaNz8z'],// 583848
					['Key' => 'l2SSZnXwk6oI3phDUgQgWfiDdWLS1Ga9', 'Secret' => 'h4G6DRPwHwUtudmT'],
					// ['Key' => '2UblDytraJrtooVm9aEGSpGQYponG8sM', 'Secret' => 'Fc9nfc0Kf9GqIeAo'] //904861					
				];
		foreach ($shortcodes as $shortcodekey => $shortcode) {
			echo "==> Starting calls on {$shortcode['orgnaization_shortcode']}\n";
			$count = 1;
			foreach ($secrets as $secretkey => $secret) {
				$object = (object)[
								'shortcode' => $shortcode['orgnaization_shortcode'],
								'consumer_key' => $secret['Key'],
								'consumer_secret' => $secret['Secret'],
							];				
				echo "\tMaking call {$count}\n";
				
				$response = $this->testRegisterCallback($object);
				// print_r($register);
				if ($response->status < 399){
					echo "\t Registered on attempt {$count} " . $object->shortcode . "\n";
				} else {
					echo "\t Failed on attempt {$count} for registration of " . $object->shortcode . " with error " . $response->body->errorMessage . "\n\n";
					$count++;
				}
				echo " " . $response->status . "\n";
				if ($response->status == 200){
					$dbshortcode = Shortcode::where('shortcode', '=', $shortcode['orgnaization_shortcode'])->get();
					if ($dbshortcode->isEmpty()){
						$dbshortcode = new Shortcode;
						$dbshortcode->short_name = $shortcode['orgnaization_name'];
						$dbshortcode->organization_id = 4;
					} else {
						$dbshortcode = $dbshortcode->first();
					}
					$dbshortcode->shortcode = $shortcode['orgnaization_shortcode'];
					$dbshortcode->consumer_key = $secret['Key'];
					$dbshortcode->consumer_secret = $secret['Secret'];
					$dbshortcode->confirm_callback = 'https://api.lipalink.com/callbacks/confirmtransaction';
					$dbshortcode->validate_callback = 'https://api.lipalink.com/callbacks/validatetransaction';
					$dbshortcode->registered = 1;
					$dbshortcode->save();
					echo "\Completed calls for {$shortcode['orgnaization_shortcode']}\n";
				}
			}
		}
		echo "==> Done \n";
	}

	public function testRegisterCallback($object)
	{
		// $this->mpesa_url = 'https://sandbox.safaricom.co.ke';
		$this->mpesa_url = env('MPESA_URL');
		$auth = $this->testAuthenticate($object->consumer_key, $object->consumer_secret)->body->access_token;
		// dd($auth);
		return $this->makeConnection('post', 'mpesa/c2b/v1/registerurl', [
						'headers' => [
							'Authorization' => 'Bearer ' . $auth,
							'Accept' => 'application/json',
						],
						'http_errors' => false,
						'debug' => false,
						'json' => [
							"ShortCode" => "$object->shortcode",
						    "ResponseType" => "Completed",
						    "ConfirmationURL" => "https://api.lipalink.com/callbacks/confirmtransaction",
						    "ValidationURL" => "https://api.lipalink.com/callbacks/validatetransaction"
						]
					]);
	}

	public function testGetToken($shortcode)
	{
		$response = $this->testAuthenticate($shortcode->consumer_key, $shortcode->consumer_secret);
		if ($response->status == 200)
			return $response->body->access_token;
		return null;
	}

	private function testAuthenticate($key, $secret)
	{
		$client = new Client(['base_uri' => $this->mpesa_url]);

		return $this->makeConnection('get', '/oauth/v1/generate?grant_type=client_credentials', [
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . base64_encode($key . ':' . $secret),
			],
			'http_errors' => false,
			'debug' => false,
		]);
	}

	private function makeConnection($method, $route, $request)
	{
		$client = new Client(['base_uri' => $this->mpesa_url]);
		try {
			$response = $client->request($method, $route, $request);
			return (object)[
				'status' => $response->getStatusCode(),
				'body' => json_decode($response->getBody()),
			];
		} catch (ClientException $e) { //Client exceptions
			return (object)[
				'status' => 400,
				'body' => Psr7\str($e->getResponse())
			];
		} catch (RequestException $e) { // In the event of a networking error (connection timeout, DNS errors, etc.)
			if ($e->hasResponse()) {
		        $message = Psr7\str($e->getResponse());
		    } else {
		    	$message = 'Could not connect to the specified host';
		    }
		    return (object)[
		    	'status' => 500,
		    	'body' => $message
		    ];
		}
	}

}