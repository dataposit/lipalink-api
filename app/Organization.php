<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['organization_name', 'address'];    

    public function paybills() {
        return $this->hasMany('App\Shortcode');
    }

    public function smsGateways()
    {
    	return $this->hasMany('App\GatewayDetail', 'organization_id');
    }

    public function smstemplate()
    {
    	return $this->hasOne('App\MessageTemplate');
    }

    public function navDetails()
    {
        return $this->hasOne('App\NavDetails', 'organization_id');
    }
}
