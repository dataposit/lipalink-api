<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayOperation extends BaseModel
{
    public function masteroperation()
    {
    	return $this->belongsTo('App\MasterGatewayOperations', 'master_gateway_operation_id');
    }
}
