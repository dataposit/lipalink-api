<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcode extends Model
{
    protected $guarded = [];

    public function transactions() {
    	return $this->hasMany('App\Payment');
    }

    public function organization()
    {
    	return $this->belongsTo('App\Organization');
    }
}
