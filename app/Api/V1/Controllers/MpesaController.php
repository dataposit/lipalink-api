<?php

namespace App\Api\V1\Controllers;

// use BaseController;
use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Requests\MpesaRequest;
use Exception;
use App\Lookup;
use App\Payment;
use App\User;
use App\SMS;
use App\Shortcode;
use App\Mpesa;
use App\ViewSmsPlaceholder;

class MpesaController extends BaseController
{

	public function queue_timeout(MpesaRequest $request)
	{
		$this->dump_log('mpesa_queue_timeout');
	}

	// Mpesa balance response
	public function balance(MpesaRequest $request)
	{
		$r = $this->dump_log('mpesa_balance');
		$result = $r->Result;

		if($result->ResultType == 0 && $result->ResultCode == 0){
			$bal = $result->ResultParameters->ResultParameter[0]->Value;

			$values = explode('|', $bal);

			$v = \App\Variable::first();
			if(!$v) $v = new Variable;

			$data = [
				'last_refreshed' => $this->convert_time($result->ResultParameters->ResultParameter[1]->Value),
				'working_account' => $values[2],
				'float_account' => $values[7],
				'utility_account' => $values[12],
				'charges_paid' => $values[17],
				'org_settlement_account' => $values[22],
			];	

			$v->fill($data);
			$v->save();

			die();
		}
	}

	// Mpesa
	public function result(MpesaRequest $request)
	{
		$r = $this->dump_log('mpesa_result_two');
		die();
		$result = $r->Result;

		if($result->ResultType == 0 && $result->ResultCode == 0){
			$bal = $result->ResultParameters->ResultParameter[0]->Value;
			// $d = $result->ResultParameters->ResultParameter[1]->Value;
			// $d = \DateTime::createFromFormat('YmdHis', $d);

			$values = explode('|', $bal);

			$v = \App\Variable::first();

			$data = [
				// 'last_refreshed' => $d->format('Y-m-d H:i:s'),
				'last_refreshed' => $this->convert_time($result->ResultParameters->ResultParameter[1]->Value),
				'working_account' => $values[2],
				'float_account' => $values[7],
				'utility_account' => $values[12],
				'charges_paid' => $values[17],
				'org_settlement_account' => $values[22],
			];	

			$v->fill($data);
			$v->save();

			die();
		}
	}

	public function confirm(MpesaRequest $request)
	{
		$body = $this->dump_log('mpesa_confirm');
		$shortcode = Shortcode::where('shortcode', $request->input('BusinessShortCode'))->first();
		if (isset($shortcode->id)) {
			$shortcode->balance = $request->input('OrgAccountBalance');
			$shortcode->save();
		}
		$paymentData = [
					'payment_code' => $request->input('TransID'),
					'shortcode_id' => $shortcode->id ?? null,
					'mobile' => $request->input('MSISDN'),
					'bill_reference' => $request->input('BillRefNumber'),
					'payment_amount' => $request->input('TransAmount'),
					'name' => $request->input('FirstName') . ' ' . $request->input('MiddleName') .  ' ' . $request->input('LastName'),
					'payment_time' => $this->convert_time($request->input('TransTime')),
					'payment_status_id' => 2,
					'transaction_type' => $request->input('TransactionType'),
					'invoice_number' => $request->input('InvoiceNumber'),
				];
		$p = new Payment;
		$p->fill($paymentData);
		$p->saveAndPush();
		$phone = $request->input('MSISDN') ?? null;
		$message = $this->getSmsTemplate($shortcode, $p);
		if (isset($message)){
			if (!($phone == '254708374149' || $phone == null))
				SMS::sendSMS($phone, $message, $shortcode->organization);
		}	
		return response()->json(['status' => 'ok'],200);	
	}


	// Incoming mpesa transactions
	public function validate_transaction(MpesaRequest $request)
	{
		$this->dump_log('mpesa_validate');
		$shortcode = Shortcode::where('shortcode', $request->input('BusinessShortCode'))->first();
		if (isset($shortcode->id)) {
			$shortcode->balance = $request->input('OrgAccountBalance');
			$shortcode->save();
		}

		if (!isset($shortcode))
			return response()->json(['status' => 'error', 'message' => 'Shortcode not found'], 404);
		$p = new Payment;
		$p->fill([
			'payment_code' => $request->input('TransID'),
			'shortcode_id' => $shortcode->id,
			'mobile' => $request->input('MSISDN'),
			'bill_reference' => $request->input('BillRefNumber'),
			'payment_amount' => $request->input('TransAmount'),
			'name' => $request->input('FirstName') . ' ' . $request->input('MiddleName') .  ' ' . $request->input('LastName'),
			'payment_time' => $this->convert_time($request->input('TransTime')),
			'payment_status_id' => 2,
			'transaction_type' => $request->input('TransactionType'),
			'invoice_number' => $request->input('InvoiceNumber'),
		]);
		
		if(starts_with($p->bill_reference, 'acc-')){
			$u = User::find(str_after($p->bill_reference, 'acc-'));
			if($u) $p->user_id = $u->id;
		}

		$p->saveAndPush();

		$phone = $request->input('MSISDN') ?? null;
		$message = $this->getSmsTemplate($shortcode, $p);
		if (isset($message)){
			if (!($phone == '254708374149' || $phone == null))
				SMS::sendSMS($phone, $message, $shortcode->organization);
		}		
		return response()->json(['status' => 'ok'],200);
	}

	// Being the callback after a lipa na mpesa query
	public function callback(MpesaRequest $request)
	{
		$body = $this->dump_log('mpesa_callback');

		$body = $body->Body->stkCallback;
		$p = Payment::where(['checkout_request_id' => $body->CheckoutRequestID, 'merchant_request_id' => $body->MerchantRequestID])->first();

		if($body->ResultCode != 0){
			$p->payment_status_id = 3;
			$p->save();
		}
		else{
			$values = $body->CallbackMetadata->Item;
			$p->fill([
				'payment_time' => $values[3]->Value,
				'payment_code' => $values[1]->Value,
				'payment_status_id' => 2,
			]);
			$p->saveAndPush();
			$message = $this->getSmsTemplate($p->shortcode, $p);
			if (isset($message)){
				$phone = $values[4]->Value;
				if (!($phone == '254708374149' || $phone == null))
					SMS::sendSMS($values[4]->Value, $message, $p->shortcode->organization);
			}
		}
	}

	private function getSmsTemplate($shortcode, $payment)
	{
		$smsTemplate = $shortcode->organization->smstemplate;
		if (!isset($smsTemplate))
			return null;
		return $smsTemplate->getMessage(ViewSmsPlaceholder::find($payment->id));
	}


	/*
	 * MPESA API calls
	 *
	 *
	 *
	 */

	public function registerCallback(MpesaRequest $request)
	{
		// Checking the validity of the request
		if (empty($request->all()))
			return response()->json(['error' => "Unsupported content type", 'status' => 415]);
		if (!$request->has('shortcode'))
			return response()->json(['error' => "Short Code property not upplied", 'status' => 422]);

		// Checking the existence of shortcode
		$getshortcode = Shortcode::where('shortcode', $request->input('shortcode'))->get();
		if ($getshortcode->isEmpty())
			return response()->json(['error' => "Supplied short code does not exist", 'status' => 412]);

		// Checking access to the short code
		/*$access_to_shortcode = auth()->user()->organization->paybills->where('id', $getshortcode->first()->id);
		if ($access_to_shortcode->isEmpty())
			return response()->json(['error' => "Access denied to the shortcode"], 403);*/

		$shortcode = $getshortcode->first();
		$mpesaclient = new Mpesa(['mpesa_url' => env('MPESA_URL')]);
		$mpesacall = $mpesaclient->registercallback($shortcode, [
						'debug' => false,
						'http_errors' => false,
						'body' => [
							'ShortCode' => $shortcode->shortcode,
							'ResponseType' => 'Completed',
							'ConfirmationURL' => $shortcode->confirm_callback,
							'ValidationURL' => $shortcode->validate_callback,
						]
					]);
		// $this->dump_log('registercallback', $mpesacall);
		return response()->json($mpesacall);
	}

	public function simulatepayment(MpesaRequest $request)
	{
		if (empty($request->all()))
			return response()->json(['error' => "Unsupported content type"], 415);
		if (!$request->has('shortcode'))
			return response()->json(['error' => "Short Code property not upplied"], 422);

		// Checking the existence of shortcode
		$getshortcode = Shortcode::where('shortcode', $request->input('shortcode'))->get();
		if ($getshortcode->isEmpty())
			return response()->json(['error' => "Supplied short code does not exist"], 412);

		// Checking access to the short code
		/*$access_to_shortcode = auth()->user()->organization->paybills->where('id', $getshortcode->first()->id);
		if ($access_to_shortcode->isEmpty())
			return response()->json(['error' => "Access denied to the shortcode"], 403);*/

		$shortcode = $getshortcode->first();
		$mpesaclient = new Mpesa(['mpesa_url' => env('MPESA_URL')]);
		$mpesacall = $mpesaclient->c2b($shortcode, [
						'debug' => false,
						'http_errors' => false,
						'body' => [
							'ShortCode' => $shortcode->shortcode,
							'CommandID' => 'CustomerPayBillOnline',
							'Amount' => rand(500, 20000),
							'Msisdn' => '254708374149',
							'BillRefNumber' => '3',
						]
					]);

		return response()->json($mpesacall);
	}

	public function stkpush(MpesaRequest $request)
	{
		// return response()->json(['data' => 'New Data']);
		return response()->json($request->all());
	}

	public function testauthentication($shortcode)
	{
		$shortcode = Shortcode::where('shortcode', '=', $shortcode)->get();
		if ($shortcode->isEmpty())
			return false;
		$mpesaclient = new Mpesa(['mpesa_url' => env('MPESA_URL')]);
		print_r($mpesaclient->authenticate($shortcode->first()));
	}

	public function testregistercallback($shortcode)
	{
		$shortcode = Shortcode::where('shortcode', '=', $shortcode)->get();
		
		if ($shortcode->isEmpty())
			return false;
		$shortcode = $shortcode->first();
		$mpesaclient = new Mpesa(['mpesa_url' => env('MPESA_URL')]);
		$response = $mpesaclient->registercallback($shortcode, [
						'debug' => true,
						'http_errors' => false,
						'body' => [
							'ShortCode' => $shortcode->shortcode,
							'ResponseType' => 'Completed',
							'ConfirmationURL' => $shortcode->confirm_callback,
							'ValidationURL' => $shortcode->validate_callback,
						]
					]);
		$response = (object) $response;
		if ($response->status < 399){
			$shortcode->registered = 1;
			$shortcode->save();
		}
		print_r($response);
	}

	public function testc2b($shortcode, $receivingShortcode, $amount = null)
	{
		$shortcode = Shortcode::where('shortcode', '=', $shortcode)->get();
		if ($shortcode->isEmpty())
			return false;
		$shortcode = $shortcode->first();
		$mpesaclient = new Mpesa(['mpesa_url' => env('MPESA_URL')]);
		$response = $mpesaclient->registercallback($shortcode, [
						'debug' => true,
						'http_errors' => false,
						'body' => [
							"Initiator" => " ",
							"SecurityCredential" => " ",
							"CommandID" => "MerchantToMerchantTransfer",
							"SenderIdentifierType" => 4,
							"RecieverIdentifierType" => 4,
							"Amount" =>  $amount ?? 1,
							"PartyA" => $shortcode->shortcode,
							"PartyB" => $receivingShortcode,
							"AccountReference" => " ",
							"Remarks" => " ",
							"QueueTimeOutURL" => "http://your_timeout_url",
							"ResultURL" => "http://your_result_url"
						]
					]);
	}

}