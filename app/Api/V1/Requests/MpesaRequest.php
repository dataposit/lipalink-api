<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class MpesaRequest extends FormRequest
{

    public function authorize()
    {
        return true;   
    }

    public function messages()
    {
        return [
            'before_or_equal' => 'The :attribute field must be before or equal to today.'
        ];
    }

    public function rules()
    {
        return [];
    }
}
