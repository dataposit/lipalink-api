<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewSmsPlaceholder extends Model
{
    protected $table = 'view_sms_placeholders';
}
