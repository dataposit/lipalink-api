<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayDetail extends BaseModel
{
    public function gateway()
    {
    	return $this->belongsTo('App\Gateway');
    }
}
