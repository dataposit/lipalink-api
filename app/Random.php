<?php

namespace App;

class Random
{
    public static function resetOrganizationTransactions($organization)
    {
    	echo "==> Checking if the organization exists\n";
    	$organization = Organization::find($organization);
    	if (empty($organization)){
    		echo "==> Organization specified does not exist\n";
    		return null;
    	}
    	echo "==> Getting the organization shortcodes\n";
    	foreach ($organization->paybills as $key => $paybill) {
    		echo "\t Resetting transactions for the paybill " . $paybill->shortcode . "\n";
    		foreach ($paybill->transactions as $key => $transaction) {
    			if ($transaction->synched())
    				$transaction->reset();
    		}
    	}
    	echo "==> Completed resetting the transactions\n";
    }
}
