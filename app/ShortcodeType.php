<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortcodeType extends Model
{
    protected $guarded = [];
}
