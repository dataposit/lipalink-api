<?php

namespace App;

use App\BaseModel;
use App\Api\V1\Controllers\BaseController;

class Payment extends BaseModel
{
    protected $fillable = [
        'user_id', 'mobile', 'name', 'email', 'shortcode_id', 'payment_type_id', 'payment_amount', 'payment_code', 'bill_reference', 'merchant_request_id', 'checkout_request_id', 'payment_status_id', 'payment_time'
    ];
    
    public function getTransactions() {
    	if (auth()->user()->user_type_id == 1){
    		return $this->all();
    	} else {
    		$transactions = auth()->user()->organization->paybills;
    		return $transactions;
    	}
    }

    public function shortcode()
    {
    	return $this->belongsTo('App\Shortcode');
    }

    public function saveAndPush()
    {
        $this->save();
        $organization = $this->shortcode->organization;

        if ($organization->push_nav == 1) {
            $Navclient = $this->getNavClient($organization->navDetails);
            $shortcode = $this->shortcode->shortcode;
            
            $payment_time = date('Y-m-d H:i:s', strtotime('-3 hours', strtotime($this->payment_time))); // Sorting the NAV display issue (Quick fix not recommended)
            $paymenttime = date('Y-m-d', strtotime($payment_time)) . "T" .date('H:i', strtotime($payment_time));
            
            $uploadData = ['id' => $this->id, "mobile" => "$this->mobile", 'name' => "$this->name",
                            'shortcode' => "$shortcode", 'payment_amount' => "$this->payment_amount",
                            'payment_code' => "$this->payment_code", 'bill_reference' => "$this->bill_reference", 'merchant_request_id' => "$this->merchant_request_id", 'checkout_request_id' => "$this->checkout_request_id",  'invoice_number' => "$this->invoice_number", 
                            "payment_time" => "$paymenttime"];
            
            $response = $Navclient->createResource($organization->navDetails->NTLM_WEBSERVICE, $uploadData);
            $base = new BaseController;
            $base->dump_log('nav_response', $response);
            if (null !== $response->error){
                $this->synched = 2;
                $this->save();
            } else {
                $this->synched = 1;
                $this->datesynched = date('Y-m-d H:i:s');
                $this->save();
            }
        }
        
        return $this;
    }

    public function synched()
    {
        if ($this->synched != 0)
            return true;
        return false;
    }

    public function onlyreset()
    {
        $this->synched = 0;
        $this->datesynched = NULL;
    }

    public function reset()
    {
        $this->onlyreset();
        return $this->save();
    }

    public function resetAndPush()
    {
        $this->onlyreset();
        return $this->saveAndPush();
    }

    public function getNavTransactions()
    {
        $Navclient = $this->getNavClient();
        $response = $Navclient->getResource('LipaLinkLodgement');
        dd($response);
    }

    public function pushTransactionToNav()
    {
        $Navclient = $this->getNavClient($this->testDetails());
        $transactions = $this->where('synched', '=', 0)->get();
        
        foreach ($transactions as $key => $transaction) { 
            $transaction->saveAndPush();           
        }
    }

    private function getNavClient($navDetails)
    {
        return new Nav([
                    'NTLM_URI' => $navDetails->NTLM_URI,
                    'NTLM_PORT' => $navDetails->NTLM_PORT,
                    'NTLM_SERVICE' => $navDetails->NTLM_SERVICE,
                    'NTLM_ODATA' => $navDetails->NTLM_ODATA,
                    'NTLM_ODATA_VERSION' => $navDetails->NTLM_ODATA_VERSION,
                    'NTLM_COMPANY' => $navDetails->NTLM_COMPANY,
                    'NTLM_USERNAME' => $navDetails->NTLM_USERNAME,
                    'NTLM_PASSWORD' => $navDetails->NTLM_PASSWORD,
                ]);
    }

    private function testDetails()
    {
        return (object)[
                    'NTLM_URI' => 'http://41.217.220.114',
                    'NTLM_PORT' => '7048',
                    'NTLM_SERVICE' => 'DynamicsNAV80',
                    'NTLM_ODATA' => 1,
                    'NTLM_ODATA_VERSION' => NULL,
                    'NTLM_COMPANY' => 'Test Glacier Products LtdKenya',
                    'NTLM_USERNAME' => 'walter.orando',
                    'NTLM_PASSWORD' => 'walter.321'];
    }
}