<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends BaseModel
{
    public function operations()
    {
    	return $this->hasMany('App\GatewayOperation');
    }
}
