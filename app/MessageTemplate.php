<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class MessageTemplate extends BaseModel
{
	function __construct()
	{
		parent::__construct();
	}
    public function getMessage($payment)
    {
    	$newMessage = null;
    	foreach ($this->getFields() as $key => $value) {
    		if (!isset($newMessage))
				$newMessage = $this->message;
			$searchString = '{$'.$value.'}';
			$newMessage = str_replace($searchString, $payment->$value, $newMessage);
    	}
		return $newMessage;
    }

    private function getFields()
    {
    	$fields = [];
    	foreach (DB::select('describe view_sms_placeholders') as $key => $column) {
    		$fields[] = $column->Field;
    	}
    	return $fields;
    }
}
