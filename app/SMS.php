<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use AfricasTalking\SDK\AfricasTalking;

class SMS
{
    public static function sendSMS($phone_number=NULL, $message=NULL, $organization) {
    	if ($organization->push_sms == 0)
    		return null;
    	$smsDetails = $organization->smsGateways;
    	if (!isset($smsDetails) || $smsDetails->isEmpty())
    		return null;
    	if (!isset($message))
    		return null;

    	$smsgateway = $smsDetails->where('default', 1);
    	if ($smsgateway->isEmpty())
    		return null;
    	$smsgateway = $smsgateway->first();
    	if ($smsgateway->gateway->id == 1) {
    		return self::sendInfoBip($smsgateway, $phone_number, $message);
    	} else if ($smsgateway->gateway->id == 2) {
    		return self::sendAfricasTalking($smsgateway, $phone_number, $message);
    	}
    }

    private static function sendInfoBip($gateway, $phone_number, $message)
    {
    	$token = base64_encode($gateway->username . ':' . $gateway->password);
    	$client = new Client(['base_uri' => $gateway->url]);
        $response = $client->request('post', '/sms/2/text/single', [
			'debug' => true,
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . $token,
			],
			'http_errors' => true,
			'json' => [
				'from' => $gateway->sender_id,
				'to' => $phone_number ?? NULL,
				'text' => $message ?? NULL,
			],
		]);

        return $response->getStatusCode();
        /*print_r($response->getStatusCode());echo "<br />";
		$body = json_decode($response->getBody());
		print_r($body);*/
    }

    private static function sendAfricasTalking($gateway, $phone_number, $message)
    {
    	$AT       = new AfricasTalking($gateway->username, $gateway->api_key);
    	$sms      = (object)$AT->sms()->send([
						    'to'      => $phone_number,
						    'message' => $message
						]);
    	if ($sms->status == 'success')
    		return 200;
    	else 
    		return 401;
    }

    public static function testSMS($phone_number=NULL, $organization, $payment) {
    	if ($organization->push_sms == 0)
    		return null;
    	$smsDetails = $organization->smsGateways;
    	if (!isset($smsDetails) || empty($smsDetails))
    		return null;
    	$template = $organization->smstemplate;
    	$message = $template->getMessage(ViewSmsPlaceholder::find($payment));
    	dd(self::sendSMS($phone_number, $message, $organization));
    }

    public static function testfromconsole()
    {
    	return self::testSMS('254725455925', Organization::find(3), 53);
    }
    

    public function testSMS2()
    {
        /*$parameters['message_type']= 'Receipt';
        $parameters['message_content'] = 'Hello ';
        $parameters['phone_number'] = '0725455925';
        $array = array('message_parameters' => $parameters);
        $data_string = json_encode($array);
        $url = 'https://simplex-financials.com/city-eye/messaging/send_appointment_messages';
        try{
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            print_r($result);
        }catch(Exception $e){
            $response = "something went wrong";
            echo json_encode($response.' '.$e);
        }*/
        $method = 'post';
        $route = '/send_appointment_messages';
        $request = [
                'headers' => [
                    'Content-Length' => 119,
                    'Content-Type' => 'application/json',
                ],
                'http_errors' => true,
                'debug' => true,
                'json' => [
                    'message_parameters' => [
                                            'message_type' => 'Receipt',
                                            'message_content' => 'Hello, DataposIT test',
                                            'phone_number' => '0725455925'
                                        ],
                    ]
            ];

        $client = new Client(['base_uri' => 'https://simplex-financials.com/city-eye/messaging']);
        try {
            $response = $client->request($method, $route, $request);
            return (object)[
                'status' => $response->getStatusCode(),
                'body' => json_decode($response->getBody()),
            ];
        } catch (ClientException $e) { //Client exceptions
            return (object)[
                'status' => 400,
                'body' => Psr7\str($e->getResponse())
            ];
        } catch (RequestException $e) { // In the event of a networking error (connection timeout, DNS errors, etc.)
            if ($e->hasResponse()) {
                $message = Psr7\str($e->getResponse());
            } else {
                $message = 'Could not connect to the specified host';
            }
            return (object)[
                'status' => 500,
                'body' => $message
            ];
        }
    }

    public function test1809()
    {
        // update these parameters
        $parameters['message_type']= 'Receipt'; // message types should be either 1. Receipt 2. Appointment 3. Invoice
        $parameters['message_content'] = 'Hello '; // this is the message that needs to be send to the client
        $parameters['phone_number'] = '0725455925'; // The recepient phone number
        $array = array('message_parameters' => $parameters);

        $data_string = json_encode($array);

        $url = 'https://simplex-financials.com/city-eye/messaging/send_appointment_messages';

        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            var_dump($result);die();
            // result is in json, so you can decode the same so that you can retrieve the reponse from sending the data.
            curl_close($ch);
        } catch(Exception $e) {
            $response = "something went wrong";
            echo json_encode($response.' '.$e);
        }
    }

}
