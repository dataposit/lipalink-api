@extends('layouts.app')

@section('content')
<div class="col-md-6">
    <h2 class="font-bold">Welcome to IN+</h2>

    <p>
        Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
    </p>

    <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
    </p>

    <p>
        When an unknown printer took a galley of type and scrambled it to make a type specimen book.
    </p>

    <p>
        <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
    </p>

</div>
<div class="col-md-6">
    <form class="m-t" role="form" action="{{ route('login') }}" method="post">
        @csrf
        <div class="form-group">
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="current-password">

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

        @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                <small>{{ __('Forgot Your Password?') }}</small>
            </a>
        @endif

        <p class="text-muted text-center">
            <small>Do not have an account?</small>
        </p>
        <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Create an account</a>
    </form>
    <p class="m-t">
        <small> &copy; 2014</small>
    </p>
</div>
@endsection
