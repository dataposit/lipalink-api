<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.9.2/empty_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Jun 2019 13:21:29 GMT -->
<head>

    <meta charset="utf-8">
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    @yield('css_scripts')

    <link href="{{ asset('template/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">

</head>

<body class="@guest gray-bg @endguest">
    @guest
        <div class="loginColumns animated fadeInDown">
            <div class="row">
                @yield('content')
            </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    Copyright Example Company
                </div>
                <div class="col-md-6 text-right">
                   <small>© 2014-2015</small>
                </div>
            </div>
        </div>
    @else
        @isset(Auth::user()->email_verified_at)
            <div id="wrapper">
                <nav class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav metismenu" id="side-menu">
                        @if(Auth::user()->isSysAdmin())
                            <li>
                                <a href="{{ url('organization') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Organizations</span></a>
                            </li>
                        @endif
                            <li>
                                <a href="{{ url('paybill') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Paybills</span></a>
                            </li>
                            <li>
                                <a href="{{ url('transactions') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Transactions</span></a>
                            </li>
                            <li>
                                <a href="{{ url('tests') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Tests</span></a>
                            </li>
                            <li>
                                <a href="{{ url('setups') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Setups</span></a>
                            </li>
                        </ul>

                    </div>
                </nav>

                <div id="page-wrapper" class="gray-bg">
                    <div class="row border-bottom">
                    <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        {{-- <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.9.2/search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form> --}}
                    </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>

                    </nav>
                    </div>

                    <div class="wrapper wrapper-content">
                        @yield('content')
                    </div>
                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>

                </div>
            </div>
        @else
            <div class="loginColumns animated fadeInDown">
                <div class="row">
                    @yield('content')
                </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-6">
                        Copyright Example Company
                    </div>
                    <div class="col-md-6 text-right">
                       <small>© 2014-2015</small>
                    </div>
                </div>
            </div>
        @endisset
    @endguest


    <!-- Mainly scripts -->
    <script src="{{ asset('template/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('template/js/popper.min.js') }}"></script>
    <script src="{{ asset('template/js/bootstrap.js') }}"></script>
    <script src="{{ asset('template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    
    @yield('js_scripts')

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('template/js/inspinia.js') }}"></script>
    <script src="{{ asset('template/js/plugins/pace/pace.min.js') }}"></script>

</body>

</html>
